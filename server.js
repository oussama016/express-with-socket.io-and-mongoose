const express = require('express')
const http = require('http')
const { Server } = require("socket.io")

const app = express()
const server = http.createServer(app)
const io = new Server(server);

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/resources/view/index.html')
})

app.get('/chat', (req, res) => {
    res.sendFile(__dirname + '/resources/view/chat.html')
})

io.on('connection', (socket) => {
    console.log('a user connected')

    socket.on('chat message', value => {
        console.log('a message from user')
        socket.broadcast.emit('chat message', value)
    })

    socket.on('disconnect', () => {
        console.log('user disconnected');
    })
})

server.listen(5000, () => {
    console.log('Server start on port 5000')
})